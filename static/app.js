const urlGetService = '/api/v1/counters';
const urlPostAndDeleteService = '/api/v1/counter';
var myEle = document.getElementById("main-content");
var list = document.querySelectorAll(".card");
var cTitle = document.querySelectorAll(".card__title");
var cImage = document.querySelectorAll(".card__image");
var cCounter = document.querySelectorAll(".card__counter");
var cBtnWrapper = document.querySelectorAll(".card__btn-wrapper");
var cBtn = document.querySelectorAll(".card__btn");
var cId = document.querySelectorAll(".card__id");
var gridView = document.querySelector("#grid-view");
var listViews = document.querySelector("#list-view");
var btnCreate = document.querySelector("#add");
var input = document.getElementById("key");
var dataset = document.querySelector(".type-view__btn--active");
dataset = dataset.dataset.view; 
var mainType = document.querySelector(".wrapper__main");
var valueDataSet="";
 


// Recupera e imprime los datos almacenados en la BD
// y recive una parametro boleano para mostrar grid o lista
function ListarCounters(typeOfView) {
//console.log("typeOfView: "+ typeOfView);
 typeOfView = typeOfView || "grid";
  
  fetch(urlGetService,
    { method: 'get' }
  )
    .then(res => res.json())
    .then(res => {
      myEle.innerHTML = '';
      var cardItem = '';
      var dataValue=0;
     
      res.reverse().map((item, index) => {
        cardItem = `<div data-key='${item.id}' data-index='${index}' 
          class='card ${typeOfView === "grid"?"card--grid":"card--list"}'id='${item.id}'>
          <div class='card__id card__id--${typeOfView}'>id: ${item.id}</div>
          <div class='card__image card__image--${typeOfView}'>
            <img src='https://source.unsplash.com/600x600/?food,${item.id}' />
          </div>
          <div class='card__title card__title--${typeOfView}'>${item.title} </div>
          <div class='card__counter card__counter--${typeOfView}'>${item.count}</div>
          <div class='card__btn-wrapper card__btn-wrapper--${typeOfView}'>
          <div class='card__btn card__btn--${typeOfView}' onclick='DecrementCounter("${item.id}")'>
            <i class="fa fa-minus"></i>
          </div>
          <div class='card__btn card__btn--${typeOfView}' onclick='IncrementCounter("${item.id}")'>
            <i class="fa fa-plus"></i>
          </div>
          </div>
          <div class='card__btn card__btn--delete-${typeOfView}'  onclick='DeleteCounter("${item.id}")'>
          <i class="fa fa-delete"></i>Eliminar
          </div>
          </div>`;

    dataValue = parseInt(dataValue) + parseInt(res[index].count , 10) ;
    totalCounter(res.length, dataValue)
    myEle.innerHTML += cardItem;
  })
  data = res;
})
}
//recive parametro de cantidad de contadores y suma total del valor
function totalCounter(cuantity, totalValue) {
  cuantity = cuantity || 0;
  totalValue = totalValue || 0;
  var quantity = document.getElementById("quantity");
  var total = document.getElementById("totalValue");
  
  for(var i=0; i<totalValue.length; totalValue++){
    console.log(i +": "+totalValue[i] )
  }
  
  quantity.innerHTML = cuantity;
  total.innerHTML = totalValue;
 
}

//agrega un nuevo Count
function CreateCounter() {
  if (document.querySelector("#key").value !== "") {
    btnCreate.disabled = false;
    fetch(urlPostAndDeleteService, {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },

      body: JSON.stringify({ title: document.querySelector("#key").value })
    })
      .then(res => res.json())
      .then(res => {
        if (res) {
          var dataset = document.querySelector(".type-view__btn--active");
          dataset = dataset.dataset.view; 

          ListarCounters(dataset);
          document.querySelector("#key").value = "";

        }
      })
  }
}

// Elimina un contador
function DeleteCounter(Idcounter) {
  fetch(urlPostAndDeleteService, {
    method: 'delete',
    body: JSON.stringify({ id: Idcounter }),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }).then(() => {
    var dataset = document.querySelector(".type-view__btn--active");
    dataset = dataset.dataset.view; 
    ListarCounters(dataset)
  }).catch(err => {
    console.error(err)
  });
  var list = document.querySelectorAll(".card");
  for (var i=0; i<list.length;i++){
    if (i===0){
      var quantity = document.getElementById("quantity");
      var total = document.getElementById("totalValue"); 
quantity.innerHTML = 0;
  total.innerHTML = 0;
    }

 
  }
  
}
// suma: aumenta en uno el valor del contador 
function IncrementCounter(Idcounter) {
  fetch(urlPostAndDeleteService + '/inc', {
    method: 'post',
    body: JSON.stringify({ id: Idcounter }),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }).then(() => {
    var dataset = document.querySelector(".type-view__btn--active");
    dataset = dataset.dataset.view; 
    ListarCounters(dataset)
  }).catch(err => {
    console.error(err)
  });
}
//resta: disminuye en uno el valor del contador 
function DecrementCounter(Idcounter) {
  fetch(urlPostAndDeleteService + '/dec', {
    method: 'post',
    body: JSON.stringify({ id: Idcounter }),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }).then(() => {
    var dataset = document.querySelector(".type-view__btn--active");
    dataset = dataset.dataset.view; 
    ListarCounters(dataset)
  }).catch(err => {
    console.error(err)
  });
}
//imprime 
ListarCounters(dataset);

// Muestra los resultados como una mosaico / grilla
function mosaicView() {
 
  listViews.classList.remove("type-view__btn--active");
  gridView.classList.add("type-view__btn--active");
for (var i = 0; i < list.length; i++) {
  console.log(list[i]);
  list[i].classList.remove("card--list");
  list[i].classList.add("card--grid");
  cTitle[i].classList.remove("card__title--list");
    cImage[i].classList.remove("card__image--list");
    cCounter[i].classList.remove("card__counter--list");
    cBtnWrapper[i].classList.remove("card__btn-wrapper--list");
    cId[i].classList.remove("card__id--list");
    cBtn[i].classList.remove("card__btn--list");
    cTitle[i].classList.add("card__title--grid");
    cImage[i].classList.add("card__image--grid");
    cCounter[i].classList.add("card__counter--grid");
    cBtnWrapper[i].classList.add("card__btn-wrapper--grid");
    cBtn[i].classList.add("card__btn--grid");
    cId[i].classList.add("card__id--grid");
}
var dataset = document.querySelector(".type-view__btn--active");
    dataset = dataset.dataset.view;
    console.log(dataset) 
     ListarCounters(dataset);
}

// Muestra los resultados como un listado
function listView() {

 listViews.classList.add("type-view__btn--active");
 gridView.classList.remove("type-view__btn--active");
  for (var i = 0; i < list.length; i++) {
    console.log(list[i]);
    list[i].classList.remove("card--grid");
    list[i].classList.add("card--list");
    cTitle[i].classList.remove("card__title--grid");
    cImage[i].classList.remove("card__image--grid");
    cCounter[i].classList.remove("card__counter--grid");
    cBtnWrapper[i].classList.remove("card__btn-wrapper--grid");
    cBtn[i].classList.remove("card__btn--grid");
    cId[i].classList.remove("card__id--grid");
    cTitle[i].classList.add("card__title--list");
    cImage[i].classList.add("card__image--list");
    cCounter[i].classList.add("card__counter--list");
    cBtnWrapper[i].classList.add("card__btn-wrapper--list");
    cBtn[i].classList.add("card__btn--list");
    cId[i].classList.add("card__id--list");
 

  }
  var dataset = document.querySelector(".type-view__btn--active");
  dataset = dataset.dataset.view;  
ListarCounters(dataset);
}

// enter activa la función createcounter
input.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) {
    event.preventDefault();
    document.getElementById("add").click();
  }
});
//comportamiento animado del header
var w="";
window.onscroll = function() {scrollFunction()};
if(window.innerWidth !== undefined ) { 
  var w = window.innerWidth;
} else {  
  var w = document.documentElement.clientWidth;
}
function scrollFunction() {

  valueDataSet= 25;
  if (document.body.scrollTop > valueDataSet || document.documentElement.scrollTop > valueDataSet) {
if (w<760){
  document.querySelector(".wrapper-add").style.top = "-20px"; 
  document.querySelector(".wrapper-add").style.width = "53%"; 
}else{
  document.querySelector(".header").style.height = "75px";
  document.querySelector(".header").style.position = "fixed";
  document.querySelector(".wrapper-add").style.position = "relative";
  document.querySelector(".wrapper-add").style.top = "-20px";
  document.querySelector(".wrapper-add").style.width = "55%"; 
}

  } else {
    document.querySelector(".header").style.height = "270px";
    document.querySelector(".header").style.position = "absolute";
    document.querySelector(".wrapper-add").style.position = "relative";
    document.querySelector(".wrapper-add").style.top = "160px"; 
    document.querySelector(".wrapper-add").style.width = "76%";  
  }
} 

scrollFunction()













